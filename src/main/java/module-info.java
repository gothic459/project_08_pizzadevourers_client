module org.pizzagui {
    requires javafx.controls;
    requires javafx.fxml;
	requires com.jfoenix;
	requires org.kordamp.ikonli.javafx;
	requires java.net.http;
	requires json;

	opens org.pizzagui to javafx.fxml;
    exports org.pizzagui;
}