package org.pizzagui;
import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class CartLabel extends javafx.scene.control.Label {

    @FXML
    private ListView<Product> cartList;

    public static ObservableList<Product> productObservableList;
    @FXML
    private JFXButton pretendToPay;
    @FXML
    private JFXButton clearCart;



    private int i = 0;


    public CartLabel(String text) {
        super();
        setText(text);
    }
    public CartLabel(){

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("cartlabel.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try{
            fxmlLoader.load();

        }catch(IOException exception){
            throw new RuntimeException(exception);
        }
        productObservableList = FXCollections.observableArrayList();
        setCart();

        clearCart.setOnMouseClicked(event -> {trashCart();});

        //onlcikevent
        pretendToPay.setOnMouseClicked(event -> {

            JSONArray orders = new JSONArray();
            JSONObject mainOrder = new JSONObject();

            for(Product product : productObservableList){

                JSONObject order = new JSONObject()
                            .put("id",product.getId())
                            .put("quantity",product.getQuant());
                    orders
                            .put(order);
            }
                 mainOrder
                        .put("order",orders);
                //System.out.println(mainOrder.toString());

            try {

                URL url = new URL("http://localhost:8080/api/pizza/order/");
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                //header
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type","application/json; utf-8");
                con.setRequestProperty("Accept", "application/json");
                //send it!
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(mainOrder.toString());
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                //System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Response Code : " + responseCode);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream())
                );
                String inputLine;
                StringBuffer response = new StringBuffer();

                while((inputLine = in.readLine()) != null){
                    response.append(inputLine);
                }
                in.close();
                trashCart();
                System.out.println(response.toString());




            } catch (IOException e) {
                e.printStackTrace();
            }

        });


        //onlcikevent

    }

//        priceAfter.setText(String.valueOf(totalPriceCart));


    public void setCart(){
        cartList.setItems(productObservableList);

        cartList.setCellFactory(new Callback<ListView<Product>, ListCell<Product>>() {
            @Override
            public ListCell<Product> call(ListView<Product> param) {
                return new ProductListViewCell();
            }
        });
        int total = 0;
        for(Product product : productObservableList){
            total += product.getTotalPrice();
        }

    }



    public static void passItem(Product product){
        productObservableList.add(product);

        // for(Product product1 : productObservableList){
        //     totalValue += (product1.getPrice() * product1.getQuant());
        // }
    }

    private void trashCart(){
        productObservableList.clear();
    }

}
