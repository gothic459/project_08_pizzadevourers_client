
package org.pizzagui;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXMasonryPane;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;
import org.json.JSONArray;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Stack;

public class Drinks implements Initializable {

    public static JSONArray drinks;
    @FXML
    public JFXMasonryPane drinkView;

    @FXML
    public StackPane stackedPane;
    public ScrollPane scroll;


    public static void setDrinks(JSONArray jsonArray){
        drinks = jsonArray;
    }




    private void addTiles(){
        scroll.setContent(drinkView);
        scroll.setFitToWidth(true);
        for(int i=0;i<drinks.length();i++){

            String name = drinks.getJSONObject(i).getString("name");
            String desc = drinks.getJSONObject(i).getString("desc");
            int price = drinks.getJSONObject(i).getInt("price")/100;
            int id = drinks.getJSONObject(i).getInt("id");
            boolean promotion_flag = drinks.getJSONObject(i).getBoolean("promotion_FLAG");

            //System.out.println(promotion_flag);
            LabelPreview label = new LabelPreview();

            label.setName(name);
            label.setPrice(price+" PLN");
            label.setImage("/images/COKE"+i+".jpg");


           if(promotion_flag){
                label.setDiscount();
            }
            //Label dialogLabel = new Label();
            MyLabel dialog = new MyLabel();
            dialog.setName(name);
            dialog.setPrice(price+" PLN");
            dialog.setPriceLiteral(String.valueOf(price));
            dialog.setProductId(String.valueOf(id));
            dialog.setImage("/images/COKE"+i+".jpg");


            dialog.setAlignment(Pos.CENTER);
            dialog.setPrefSize(512.0,512.0);
            //dialog.setStyle("-fx-background-color: #606060");

            label.setOnMouseClicked(event ->{
                JFXDialog dialogL = new JFXDialog(stackedPane,dialog,JFXDialog.DialogTransition.CENTER);
                dialogL.show();

            });

            String s = label.getStyle();
            label.setOnMouseEntered(event -> {
                label.setStyle("-fx-background-color: rgba(51,0,0,0.12)");
            });
            label.setOnMouseExited(event ->{
                label.setStyle(s);

            });
            drinkView.getChildren().add(label);


        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (drinks == null || drinks.length() == 0){
            Label label = new Label();
            label.setText("Something went wrong :(");
        }else{
            addTiles();
        }

    }
}
