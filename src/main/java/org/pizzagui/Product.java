package org.pizzagui;

public class Product {
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDiscounted() {
		return isDiscounted;
	}

	public void setDiscounted(boolean discounted) {
		isDiscounted = discounted;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getQuant() {
		return quant;
	}

	public void setQuant(int quant) {
		this.quant = quant;
	}

	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}


	private int id;
	private int price;
	private String name;
	private boolean isDiscounted;
	private String desc;
	private int quant;
	private int totalPrice;

	Product(int id,String name, int price, int quant,int totalPrice){
		this.id = id;
		this.name = name;
		this.price = price;
		this.quant = quant;
		this.totalPrice = totalPrice;
	}


}
