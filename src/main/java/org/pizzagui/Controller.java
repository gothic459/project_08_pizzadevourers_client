package org.pizzagui;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.json.JSONArray;
import org.json.JSONObject;
import org.kordamp.ikonli.javafx.FontIcon;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ResourceBundle;




public class Controller implements Initializable {

    private final HttpClient httpClient =HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();


    private static String childName;
    @FXML
    public FontIcon showCart;
    public StackPane stackedPane;
    @FXML
    private JFXHamburger hamburgerMenu;

    @FXML
    private JFXDrawer drawerMenu;
    @FXML
    private AnchorPane contentContainer;
    public Node anchorNode;





    @Override
    public void initialize(URL location, ResourceBundle resources) {


        try{
            JSONObject menu = callGet();
            JSONArray pizzas = menu.getJSONArray("pizzas");
            JSONArray drinks = menu.getJSONArray("beverages");

            Foods.setFoods(pizzas);
            Drinks.setDrinks(drinks);
            Offers.setOffers(pizzas,drinks);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {


            VBox vbox = FXMLLoader.load(getClass().getResource("sidePanel.fxml"));
            drawerMenu.setSidePane(vbox);
            drawerMenu.setResizableOnDrag(false);

            for(Node node : vbox.getChildren()){
                if(node.getAccessibleText() != null){
                    node.addEventHandler(MouseEvent.MOUSE_CLICKED,(e)->{
                        switch(node.getAccessibleText().toLowerCase()){
                            case "foods":
                                try {
                                    anchorNode = (Node)FXMLLoader.load(getClass().getResource("foods.fxml"));
                                    anchorNode.resize(1280,718);
                                    contentContainer.getChildren().setAll(anchorNode);
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                }
                                break;
                            case "drinks":
                                try {
                                    anchorNode = (Node)FXMLLoader.load(getClass().getResource("drinks.fxml"));
                                    contentContainer.getChildren().setAll(anchorNode);
                                    anchorNode.resize(1280,718);
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                }
                                break;
                            case "offers":
                                try {
                                    anchorNode = (Node)FXMLLoader.load(getClass().getResource("offers.fxml"));
                                    contentContainer.getChildren().setAll(anchorNode);
                                    anchorNode.resize(1280,718);
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                }
                                break;

                        }
                    });
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(hamburgerMenu);
        transition.setRate(-1);
        drawerMenu.open();


        hamburgerMenu.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            transition.setRate(transition.getRate()*-1);
            transition.play();
            if(drawerMenu.isOpened()){
                drawerMenu.setVisible(false);
                drawerMenu.close();
                AnchorPane.setLeftAnchor(contentContainer,90.0);
                AnchorPane.setRightAnchor(contentContainer,90.0);
                contentContainer.setStyle("-fx-alignment: center");
            }else{
                drawerMenu.open();
                drawerMenu.setVisible(true);
                AnchorPane.setLeftAnchor(contentContainer,180.0);
                contentContainer.setStyle("-fx-alignment: center");

            }
        });


            //dialog.setAlignment(Pos.CENTER);
        CartLabel cartLabel = new CartLabel();


            //dialog.setPrefSize(512.0,512.0);
        showCart.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            JFXDialog cart = new JFXDialog(stackedPane,cartLabel,JFXDialog.DialogTransition.CENTER);
            cart.show();
        });

    }

    public JSONObject callGet() throws Exception{
        HttpRequest req = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("http://localhost:8080/api/pizza/products")) //all of menu
                .build();

        HttpResponse <String> response = httpClient.send(req, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        JSONObject responseObject = new JSONObject(response.body()); //get the whole response
        JSONObject menu = (JSONObject) responseObject.get("menu"); //get the first object out...

        return menu;
    }


}
