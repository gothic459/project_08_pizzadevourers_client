package org.pizzagui;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.kordamp.ikonli.javafx.FontIcon;

import java.io.IOException;

public class LabelPreview extends javafx.scene.control.Label {

    @FXML
    private Text price;
    @FXML
    private Text name;
	@FXML
    private ImageView showcase;
    @FXML
    private FontIcon discounted;

    int i = 0;


    public LabelPreview(String text) {
        super();
        setText(text);
    }
    public LabelPreview(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("labelpreview.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try{
            fxmlLoader.load();
        }catch(IOException exception){
            throw new RuntimeException(exception);
        }

    }
    public void setPrice(String txt){
        price.setText(txt);
    }
    public void setName(String txt){
        name.setText(txt);
    };
    public void setImage(String path){
        showcase.setImage(new Image(getClass().getResourceAsStream(path)));
    }
    public void setDiscount(){
        discounted.setVisible(true);
    }


}
