package org.pizzagui;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXMasonryPane;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ResourceBundle;

public class Offers implements Initializable {
    @FXML
    public JFXMasonryPane offerView;
    @FXML
    public StackPane stackedPane;

    public static JSONArray pizzas;
    public static JSONArray drinks;

    public static void setOffers(JSONArray food, JSONArray drink){
        pizzas = food;
        drinks = drink;

    }

    private void addTiles(){
        //todo: get both arrays and only show those with promotion flag true
        for(int i=0;i<pizzas.length();i++){
            String name="";
            String desc = "";
            int price = 0;
            boolean promotion_flag_p = pizzas.getJSONObject(i).getBoolean("promotion_FLAG");
            boolean promotion_flag_d = drinks.getJSONObject(i).getBoolean("promotion_FLAG");

            if(promotion_flag_p ){
                 name = pizzas.getJSONObject(i).getString("name");
                 desc = pizzas.getJSONObject(i).getString("desc");
                 price = pizzas.getJSONObject(i).getInt("price");
            }

            if(promotion_flag_d){
                name = drinks.getJSONObject(i).getString("name");
                desc = drinks.getJSONObject(i).getString("desc");
                price = drinks.getJSONObject(i).getInt("price");
            }

            //boolean promotion_flag = pizzas.getJSONObject(i).getBoolean("promotion_FLAG");

            Label label = new Label();


            label.setText(name);
            label.setAlignment(Pos.CENTER);
            label.setPrefSize(128.0,128.0);


            //Label dialogLabel = new Label();
            MyLabel dialog = new MyLabel();
            dialog.setName(name);
            dialog.setPrice(String.valueOf(price));

            dialog.setAlignment(Pos.CENTER);
            dialog.setPrefSize(512.0,512.0);
            //dialog.setStyle("-fx-background-color: #606060");

            label.setOnMouseClicked(event ->{
                JFXDialog dialogL = new JFXDialog(stackedPane,dialog,JFXDialog.DialogTransition.CENTER);
                dialogL.show();
            });
            label.setOnMouseEntered(event -> {
                //label.setStyle("-fx-background-color: black");
                //todo: some blur effect?
            });
            label.setOnMouseExited(event ->{
                //label.setStyle("-fx-background-color: grey");
            });
            offerView.getChildren().add(label);


        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addTiles();
    }
}
