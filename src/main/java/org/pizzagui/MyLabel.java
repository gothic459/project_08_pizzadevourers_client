package org.pizzagui;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.kordamp.ikonli.javafx.FontIcon;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class MyLabel extends javafx.scene.control.Label  {
    @FXML
    private JFXButton addquant;
    @FXML
    private JFXButton subtractquant;
    @FXML
    private Text price;
    @FXML
    private Text priceLiteral;
    @FXML
    private Text productId;
    @FXML
    private Text name;
    @FXML
    private Text quantity;
    @FXML
    private JFXButton addToCart;
    @FXML
    private ImageView showcase;
    @FXML
    private FontIcon discounted;
    @FXML


    int i = 0;


    public MyLabel(String text) {
        super();
        setText(text);
    }
    public MyLabel(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("mylabel.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try{
            fxmlLoader.load();
        }catch(IOException exception){
            throw new RuntimeException(exception);
        }
        quantity.setText("0");

        addquant.setOnMouseClicked(event ->{
            i++;
            quantity.setText(String.valueOf(i));
            });

        subtractquant.setOnMouseClicked(event -> {
            if(i>0){
                i--;

            }
            quantity.setText(String.valueOf(i));

        });
        addToCart.setOnMouseClicked(event ->{
            if(i > 0 ) {
                int totalPrice = Integer.parseInt(this.priceLiteral.getText()) * Integer.parseInt(this.quantity.getText());
                Product product = new Product(Integer.parseInt(this.productId.getText()),this.name.getText(), Integer.parseInt(this.priceLiteral.getText()), Integer.parseInt(this.quantity.getText()),totalPrice);
                //System.out.println("pr id:"+Integer.parseInt(this.productId.getText()));
                //System.out.println("qu:"+Integer.parseInt(this.quantity.getText()));

                CartLabel.passItem(product);

                i=0;
                this.quantity.setText("0");
            }
        });


    }
    public void setPrice(String txt){
        price.setText(txt);
    }
    public void setPriceLiteral(String txt){
        priceLiteral.setText(txt);

    }
    public void setName(String txt){
        name.setText(txt);
    };
    public void setImage(String path) {
        showcase.setImage(new Image(getClass().getResourceAsStream(path)));
    }
    public void setProductId(String id){
        productId.setText(id);
    }

    public void setDiscount(){
        discounted.setVisible(true);
    }



}
