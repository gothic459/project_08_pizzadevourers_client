package org.pizzagui;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;

import java.io.IOException;

public class ProductListViewCell extends ListCell<Product> {
	@FXML
	private Label name;
	@FXML
	private Label price;
	@FXML
	private Label quant;
	@FXML
	private Label total;

	@FXML
	private GridPane gridPane;

	@Override
	protected void updateItem(Product product, boolean empty){
		super.updateItem(product,empty);
		if(empty || product == null){
			setText(null);
			setGraphic(null);
		}else{
			FXMLLoader mLLoader = null;
			if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("productlistviewcell.fxml"));
                mLLoader.setController(this);

                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


            name.setText(product.getName());
            price.setText(String.valueOf(product.getPrice()));
            quant.setText(String.valueOf(product.getQuant()));



			//System.out.println(product.getPrice() * product.getQuant());
            setText(null);
            setGraphic(gridPane);
        }

    }


}

