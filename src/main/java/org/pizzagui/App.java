package org.pizzagui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
/**
 * JavaFX App
 */
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException{
        scene = new Scene(loadFXML("sample"));
        stage.setTitle("Pizza shop");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();


    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }
    public static void main(String[] args) {
        launch();
    }
}
