package org.pizzagui;

import com.jfoenix.controls.*;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import org.json.JSONArray;
import org.json.JSONObject;
import org.kordamp.ikonli.javafx.FontIcon;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Stack;

public class Foods implements Initializable {

    public static JSONArray pizzas;
    @FXML
    public JFXMasonryPane foodView;
    @FXML
    public StackPane stackedPane;
    @FXML
    public AnchorPane mainAnchor;
    @FXML
    public ScrollPane scroll;

    public static boolean closeThis = false;




    public static void setFoods(JSONArray jsonArray){
        pizzas = jsonArray;
    }

    private void addTiles() throws IOException {
        scroll.setContent(foodView);
        scroll.setFitToWidth(true);
        for(int i=0;i<pizzas.length();i++){

            String name = pizzas.getJSONObject(i).getString("name");
            String desc = pizzas.getJSONObject(i).getString("desc");
            int price = pizzas.getJSONObject(i).getInt("price")/100;
            int id = pizzas.getJSONObject(i).getInt("id");
            boolean promotion_flag = pizzas.getJSONObject(i).getBoolean("promotion_FLAG");

            //System.out.println(promotion_flag);
            LabelPreview label = new LabelPreview();



            label.setName(name);
            label.setPrice(price+" PLN");
            label.setImage("/images/pizza"+i+".jpg");
            System.out.print(i);
            //System.out.println("src/main/resources/images/pizza"+i+".jpeg");
            //label.setPrefSize(128.0,128.0);
            if(promotion_flag){
                label.setDiscount();
            }

            //Label dialogLabel = new Label();

            //dialog.setStyle("-fx-background-color: #606060");
                MyLabel dialog = new MyLabel();
                dialog.setName(name);
                dialog.setPrice(price+ " PLN");
                dialog.setProductId(String.valueOf(id));
                dialog.setPriceLiteral(String.valueOf(price));
                dialog.setImage("/images/pizza"+i+".jpg");
                dialog.setAlignment(Pos.CENTER);
                dialog.setPrefSize(512.0,512.0);


            label.setOnMouseClicked(event ->{
                JFXDialog dialogL = new JFXDialog(stackedPane,dialog,JFXDialog.DialogTransition.CENTER);

                dialogL.show();
            });


            String s = label.getStyle();
            label.setOnMouseEntered(event -> {
                label.setStyle("-fx-background-color: rgba(51,0,0,0.12)");
                //todo: some blur effect?
            });
            label.setOnMouseExited(event ->{
                label.setStyle(s);
               
            });
            foodView.getChildren().add(label);


        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            if (pizzas == null || pizzas.length() == 0){
                Label label = new Label();
                label.setText("Something went wrong :(");
            }else{
                addTiles();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
